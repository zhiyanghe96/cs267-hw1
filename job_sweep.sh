#!/bin/bash -l
#SBATCH -C knl
#SBATCH -c 1
#SBATCH -S 4
#SBATCH -p debug
#SBATCH -N 1
#SBATCH -t 00:2:00
#SBATCH --cpu-freq=1400000
#SBATCH -J job-blocked
#SBATCH -o job-blocked.o%j
#SBATCH -e job-blocked.e%j

export MKL_NUM_THREADS=1

date
for row_size in 3 5 7 9 11 13 15 17 19
do
    echo "Testing row $row_size col $row_size"
    sed "s/_BLOCK_ROW_SIZE_/${row_size}/g" dgemm-blocked-x.cppx > build/dgemm-blocked-x.cpp2
    sed "s/_BLOCK_COL_SIZE_/${row_size}/g" build/dgemm-blocked-x.cpp2 > dgemm-blocked.c
    cd build
    # rm -r *.dSYM
    make clean && cmake -DCMAKE_BUILD_TYPE=Release .. > /dev/null 2>&1
    wait
    make -j 8 > /dev/null 2>&1
    wait
    srun -N 1 -n 1 ./benchmark-blocked > "../data/out_${row_size}_${row_size}.txt"
    wait
    cat "../data/out_${row_size}_${row_size}.txt" | grep Average
    cd ..
    date
done