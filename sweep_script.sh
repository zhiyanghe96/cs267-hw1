# date
# for row_size in 3 4 5 7 8 11 13 15
# do
#     for col_size in 3 4 5 7 8 11 13 15
#     # for col_size in 7
#     do
#         echo "Testing row $row_size col $col_size"
#         sed "s/{BLOCK_ROW_SIZE}/${row_size}/g" dgemm-blocked-x.cppx > build/dgemm-blocked-x.cpp2
#         sed "s/{BLOCK_COL_SIZE}/${col_size}/g" build/dgemm-blocked-x.cpp2 > dgemm-blocked.c
#         cd build
#         make clean && cmake -DCMAKE_BUILD_TYPE=Release .. > /dev/null 2>&1
#         wait
#         make -j 8 > /dev/null 2>&1
#         wait
#         ./benchmark-blocked > "../data/out_${row_size}_${row_size}.txt"
#         wait
#         cat "../data/out_${row_size}_${row_size}.txt" | grep Average
#         cd ..
#         date
#     done
# done



date
for row_size in 40 48 56 64 80 96 128 156 192 208 224 240 288
do
    echo "Testing row $row_size col $row_size"
    sed "s/_BLOCK_ROW_SIZE_/${row_size}/g" dgemm-blocked-x.cppx > build/dgemm-blocked-x.cpp2
    sed "s/_BLOCK_COL_SIZE_/${row_size}/g" build/dgemm-blocked-x.cpp2 > dgemm-blocked.c
    cd build
    # rm -r *.dSYM
    make clean && cmake -DCMAKE_BUILD_TYPE=Release .. > /dev/null 2>&1
    wait
    make -j 8 > /dev/null 2>&1
    wait
    ./benchmark-blocked > "../data/out_${row_size}_${row_size}.txt"
    wait
    cat "../data/out_${row_size}_${row_size}.txt" | grep Average
    cd ..
    date
done