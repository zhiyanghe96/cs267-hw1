// const char* dgemm_desc = "Simple blocked dgemm.";
// const char* dgemm_desc = "Block 288 avx for M multiple of 8";
#include <immintrin.h>
#include <stdlib.h>
#include <stdio.h>

// Tiled register code based on: https://upcommons.upc.edu/bitstream/handle/2117/16308/VECTORIZED+REGISTER+TILING.pdf;jsessionid=19C9E184526AF4E5EAD154B54E0715F0?sequence=1


// const char* dgemm_desc = "Simple blocked dgemm.";
const char* dgemm_desc = "Tiled spatial-cache blocked dgemm 21 x 21.";

#ifndef BLOCK_ROW_SIZE
#define BLOCK_ROW_SIZE 8
#endif
#ifndef BLOCK_COL_SIZE
#define BLOCK_COL_SIZE 8
#endif



const int FREE_OLD = 1;
const int KEEP_OLD = 0;

#define min(a, b) (((a) < (b)) ? (a) : (b))

/* Access matrix memory in column-major fashion
 * Matrix: lda_row * lda_col
*/

#define _access_memory_col(M, lda_row, lda_col, i, j) ((M)[(lda_row) * (j) + (i)])
#define _access_memory_row(M, lda_row, lda_col, i, j) ((M)[(lda_col) * (i) + (j)])

/* Access memory of block(ib, jb)[i, j] out of M (lda_row, lda_col), block size (row_blsz, col_blsz) */
#define _access_memory_block_col(M, lda_row, lda_col, row_blsz, col_blsz, ib, jb, i, j) \
    (M)[(row_blsz) * (ib) + (i) + ((lda_row)) * ((col_blsz) * (jb) + (j))]
    // row: row_blsz * ib + i, col: col_blsz * jb + j
    // idx: row + (lda_row * col)

/* Access memory of block(ib, jb)[i, j] out of M (lda_row, lda_col), block size (row_blsz, col_blsz) */
#define _access_memory_block_row(M, lda_row, lda_col, row_blsz, col_blsz, ib, jb, i, j) \
    (M)[(col_blsz) * (jb) + (j) + ((lda_col)) * ((row_blsz) * (ib) + (i))]
    // row: row_blsz * ib + i, col: col_blsz * jb + j
    // idx: col + (lda_col * row)

//double* M, int lda_row, int lda_col, int row_blsz, int col_blsz, int row, int col, int col_major
#define _jump_to_col(M, lda_row, lda_col, row_blsz, col_blsz, ib, jb) \
    ((M) + (lda_row) * ((jb) * (col_blsz)) + (ib) * (row_blsz))
    // row: row_blsz * ib, col: col_blsz * jb
    // idx: row + (lda_row * col)

//double* M, int lda_row, int lda_col, int row_blsz, int col_blsz, int row, int col, int row_major
#define _jump_to_row(M, lda_row, lda_col, row_blsz, col_blsz, ib, jb) \
    ((M) + (lda_col) * ((ib) * (row_blsz)) + (jb) * (col_blsz))
    // row: row_blsz * ib, col: col_blsz * jb
    // idx: col + (lda_col * row)

/* Return the first location of flattened block(ib, jb) out of M (lda_row, lda_col), block size (row_blsz, col_blsz) */
#define _jump_to_flatten_block_col(M, lda_row, lda_col, row_blsz, col_blsz, ib, jb) \
    ((M) + ((row_blsz) * min((col_blsz), (lda_col) - (jb) * (col_blsz)) * (ib) + ((col_blsz) * (jb)) * (lda_row)))
    // row: row_blsz * ib, col: col_blsz * jb
    // block_size: row_blsz * min(col_blsz, lda_col - jb * col_blsz)
    // idx: ib * block_size + col * lda_row

/* Return the first location of flattened block(ib, jb) out of M (lda_row, lda_col), block size (row_blsz, col_blsz) */
#define _jump_to_flatten_block_row(M, lda_row, lda_col, row_blsz, col_blsz, ib, jb) \
    ((M) + ((col_blsz) * min((row_blsz), (lda_row) - (ib) * (row_blsz)) * (jb) + ((row_blsz) * (ib)) * (lda_col)))
    // row: row_blsz * ib, col: col_blsz * jb
    // block_size: min(row_blsz, lda_row - ib * row_blsz) * col_blsz
    // idx: jb * block_size + row * lda_col


// TODO: get outer product working
// TODO: (-1): experiment with C memory layout
// TODO: immintrin.h, https://stackoverflow.com/questions/11228855/header-files-for-x86-simd-intrinsics, --march=native, -S
// TODO: https://stackoverflow.com/questions/137038/how-do-you-get-assembler-output-from-c-c-source-in-gcc
void _print_matrix(double*M, int ldar, int ldac, int col_major) {
    if (col_major) {
        for (int i = 0; i < ldar; i++) {
            for (int j = 0; j < ldac; j++) { // Plot row first
                printf("%.4f ", _access_memory_col(M, ldar, ldac, i, j)); fflush(stdout);
            }
            printf("\n"); fflush(stdout);
        }
    } else {
        for (int i = 0; i < ldar; i++) {
            for (int j = 0; j < ldac; j++) {
                printf("%.4f ", _access_memory_row(M, ldar, ldac, i, j)); fflush(stdout);
            }
            printf("\n"); fflush(stdout);
        }
    }
}

static void do_block(int lda, int M, int N, int K, double* A, double* B, double* C) {
    // For each row i of A
    for (int i = 0; i < M; ++i) {
        // For each column j of B
        for (int j = 0; j < N; ++j) {
            // Compute C(i,j)
            double cij = C[i + j * lda];
            for (int k = 0; k < K; ++k) {
                cij += A[i + k * lda] * B[k + j * lda];
            }
            C[i + j * lda] = cij;
        }
    }
}

double* _copy(double*M, int ldar, int ldac) {
    double * new_M = (double *)calloc(ldar * ldac, sizeof(double));
    for (int j = 0; j < ldac; j++) {
        for (int i = 0; i < ldar; i++) {
            double val = _access_memory_row(M, ldar, ldac, i, j);
            _access_memory_row(new_M, ldar, ldac, i, j) = val;
        }
    }
    return new_M;
}

double* _to_row_major(double*M, int ldar, int ldac, int free_old) {
    double * new_M = (double *)calloc(ldar * ldac, sizeof(double));
    for (int j = 0; j < ldac; j++) {
        for (int i = 0; i < ldar; i++) {
            double val = _access_memory_col(M, ldar, ldac, i, j);
            _access_memory_row(new_M, ldar, ldac, i, j) = val;
        }
    }
    if (free_old > 0) {
        free(M);
    }
    return new_M;
}

double* _to_col_major(double*M, int ldar, int ldac, int free_old) {
    double * new_M = (double *)calloc(ldar * ldac, sizeof(double));
    for (int i = 0; i < ldar; i++) {
        for (int j = 0; j < ldac; j++) {
            double val = _access_memory_row(M, ldar, ldac, i, j);
            _access_memory_col(new_M, ldar, ldac, i, j) = val;
        }
    }
    if (free_old > 0) {
        free(M);
    }
    return new_M;
}



double* _flatten_memory_block(double*M, int ldar, int ldac, int nb_row, int nb_col,
                              int row_blsz, int col_blsz, int col_major) {
    double * new_M = (double *)calloc(ldar * ldac, sizeof(double));
    int idx_new = 0;

    // // Loop through M's elements in col-major fashion (j -> i)
    if (col_major) {
        for (int jb = 0; jb < nb_col; jb++) {
            for (int ib = 0; ib < nb_row; ib++) {
                for (int jk = 0; jk < col_blsz; jk++) {
                    for (int ik = 0; ik < row_blsz; ik++) {
                        int i = ib * row_blsz + ik;
                        int j = jb * col_blsz + jk;
                        if (i < ldar && j < ldac) {
                            double val = _access_memory_col(M, ldar, ldac, i, j);
                            // printf("access ib(%d) jb(%d) ik(%d) jk(%d) i(%d) j(%d) %.04f\n", ib, jb, ik, jk, i, j, val); fflush(stdout);
                            new_M[idx_new] = val;
                            idx_new += 1;
                        }
                    }
                }
            }
        }
    } else {
        for (int ib = 0; ib < nb_row; ib++) {
            for (int jb = 0; jb < nb_col; jb++) {
                for (int ik = 0; ik < row_blsz; ik++) {
                    for (int jk = 0; jk < col_blsz; jk++) {
                        int i = ib * row_blsz + ik;
                        int j = jb * col_blsz + jk;
                        if (i < ldar && j < ldac) {
                            double val = _access_memory_row(M, ldar, ldac, i, j);
                            //printf("access ib(%d) jb(%d) ik(%d) jk(%d) i(%d) j(%d) %.04f\n", ib, jb, ik, jk, i, j, val); fflush(stdout);
                            new_M[idx_new] = val;
                            idx_new += 1;
                        }
                    }
                }
            }
        }
    }
    free(M);
    return new_M;
}

double* _unflatten_memory_block(double*M, int ldar, int ldac, int nb_row, int nb_col,
                                int row_blsz, int col_blsz, int col_major) {
    double * new_M = (double *)calloc(ldar * ldac, sizeof(double));
    int idx_new = 0;

    // Loop through M's elements in col-major fashion (j -> i)
    if (col_major) {
        for (int jb = 0; jb < nb_col; jb++) {
            for (int ib = 0; ib < nb_row; ib++) {
                for (int jk = 0; jk < col_blsz; jk++) {
                    for (int ik = 0; ik < row_blsz; ik++) {
                        int i = ib * row_blsz + ik;
                        int j = jb * col_blsz + jk;
                        if (i < ldar && j < ldac) {
                            double val = M[idx_new];
                            _access_memory_block_col(new_M, ldar, ldac, row_blsz, col_blsz, ib, jb, ik, jk) = val;
                            // _access_memory_col(new_M, ldar, ldac, i, j) = val;
                            idx_new += 1;
                        }
                    }
                }
            }
        }
    } else {
        for (int ib = 0; ib < nb_row; ib++) {
            for (int jb = 0; jb < nb_col; jb++) {
                for (int ik = 0; ik < row_blsz; ik++) {
                    for (int jk = 0; jk < col_blsz; jk++) {
                        int i = ib * row_blsz + ik;
                        int j = jb * col_blsz + jk;
                        if (i < ldar && j < ldac) {
                            double val = M[idx_new];
                            // Write back to M in a column-major fashion
                            _access_memory_block_row(new_M, ldar, ldac, row_blsz, col_blsz, ib, jb, ik, jk) = val;
                            idx_new += 1;
                        }
                    }
                }
            }
        }
    }
    free(M);
    return new_M;
}





__attribute__((optimize("unroll-loops")))
static void do_block_jki(int br_A, int bc_A, int br_B, int bc_B, int br_C, int bc_C,
                                   int M, int N, int K, double* A, double* B, double* C) {
    // For each col j of B
    double Aik, Bkj, Cij;
    for (int j = 0; j < N; ++j) {
        // For each iteration k (going right along A row, down B column)
        for (int k = 0; k < K; ++k) {
            for (int i = 0; i < M; ++i) {
                // Aik = _access_memory_row(A, br_A, bc_A, i, k);
                Aik = _access_memory_col(A, br_A, bc_A, i, k);
                Bkj = _access_memory_col(B, br_B, bc_B, k, j);
                Cij = _access_memory_col(C, br_C, bc_C, i, j);
                _access_memory_col(C, br_C, bc_C, i, j) = Aik * Bkj + Cij;
                // printf("Row i(%d) j(%d) k(%d) Aik(%.04f) Bkj(%.04f) Cij((%.04f) ||", i, j, k, Aik, Bkj, Cij);
            }
        }
    }
}

__attribute__((optimize("unroll-loops")))
static void do_block_ijk_row(int br_A, int bc_A, int br_B, int bc_B, int br_C, int bc_C,
                                   int M, int N, int K, double* A, double* B, double* C) {
    // For each col j of B
    double Aik, Bkj, Cij;
    for (int i = 0; i < M; ++i) {
        for (int j = 0; j < N; ++j) {
            // For each iteration k (going right along A row, down B column)
            for (int k = 0; k < K; ++k) {
                Aik = _access_memory_col(A, br_A, bc_A, i, k);
                // Aik = _access_memory_row(A, br_A, bc_A, i, k);
                Bkj = _access_memory_col(B, br_B, bc_B, k, j);
                Cij = _access_memory_col(C, br_C, bc_C, i, j);
                _access_memory_col(C, br_C, bc_C, i, j) = Aik * Bkj + Cij;
                // if ((M < 8 || K < 8) && (N < 8 || K < 8) ) {
                // printf("Row i(%d) j(%d) k(%d) Aik(%.04f) Bkj(%.04f) Cij((%.04f) ||", i, j, k, Aik, Bkj, Cij);
                // }
            }
        }
    }
    // printf("\n");
}

__attribute__((optimize("unroll-loops")))
static void do_block_ijk_col(int br_A, int bc_A, int br_B, int bc_B, int br_C, int bc_C,
                                   int M, int N, int K, double* A, double* B, double* C) {
    // For each col j of B
    double Aik, Bkj, Cij;
    for (int i = 0; i < M; ++i) {
        for (int j = 0; j < N; ++j) {
            // For each iteration k (going right along A row, down B column)
            for (int k = 0; k < K; ++k) {
                Aik = _access_memory_col(A, br_A, bc_A, i, k);
                Bkj = _access_memory_col(B, br_B, bc_B, k, j);
                Cij = _access_memory_col(C, br_C, bc_C, i, j);
                _access_memory_col(C, br_C, bc_C, i, j) = Aik * Bkj + Cij;
                // if ((M < 8 || K < 8) && (N < 8 || K < 8) ) {
                // printf("Col i(%d) j(%d) k(%d) Aik(%.04f) Bkj(%.04f) Cij((%.04f) ||", i, j, k, Aik, Bkj, Cij);
                // }
            }
        }
    }
    // printf("\n");
}

__attribute__((optimize("unroll-loops")))
// static void do_block_avx(int lda, int M, int N, int K, double* A, double* B, double* C) {
static void do_block_avx_row(int br_A, int bc_A, int br_B, int bc_B, int br_C, int bc_C,
                                   int M, int N, int K, double* A, double* B, double* C) {
    // For each col j of B
    int jlda; int klda; double bkj;
    for (int j = 0; j < N; ++j) {
        jlda = j * br_B;
        // For each iteration k (going right along A row, down B column)
        for (int k = 0; k < K; k = ++k) {
            __m512d Bkjlda = _mm512_set1_pd(_access_memory_col(B, br_B, bc_B, k, j));
            for (int i = 0; i < M; i = i + 8) {
                __m512d Cijlda = _mm512_loadu_pd(&_access_memory_col(C, br_C, bc_C, i, j)); //mask instruction will handle edge case
                __m512d Aiklda = _mm512_loadu_pd(&_access_memory_row(A, br_A, bc_A, i, k));
                _mm512_storeu_pd(&_access_memory_col(C, br_C, bc_C, i, j), _mm512_fmadd_pd(Aiklda, Bkjlda, Cijlda));
            }
        }
    }
}


__attribute__((optimize("unroll-loops")))
// static void do_block_avx(int lda, int M, int N, int K, double* A, double* B, double* C) {
static void do_block_avx_col(int br_A, int bc_A, int br_B, int bc_B, int br_C, int bc_C,
                                   int M, int N, int K, double* A, double* B, double* C) {
    // For each col j of B
    int jlda; int klda; double bkj;
    for (int j = 0; j < N; ++j) {
        jlda = j * br_B;
        // For each iteration k (going right along A row, down B column)
        for (int k = 0; k < K; k = ++k) {
            __m512d Bkjlda = _mm512_set1_pd(_access_memory_col(B, br_B, bc_B, k, j));
            for (int i = 0; i < M; i = i + 8) {
                __m512d Cijlda = _mm512_loadu_pd(&_access_memory_col(C, br_C, bc_C, i, j)); //mask instruction will handle edge case
                __m512d Aiklda = _mm512_loadu_pd(&_access_memory_col(A, br_A, bc_A, i, k));
                _mm512_storeu_pd(&_access_memory_col(C, br_C, bc_C, i, j), _mm512_fmadd_pd(Aiklda, Bkjlda, Cijlda));
            }
        }
    }
}




// Tiled register code based on: https://upcommons.upc.edu/bitstream/handle/2117/16308/VECTORIZED+REGISTER+TILING.pdf;jsessionid=19C9E184526AF4E5EAD154B54E0715F0?sequence=1
// TODO: https://stackoverflow.com/questions/137038/how-do-you-get-assembler-output-from-c-c-source-in-gcc
/*
 * This auxiliary subroutine performs a smaller dgemm operation
 *  C := C + A * B
 * where C is M-by-N, A is M-by-K, and B is K-by-N.
 */
static void do_tiling_kernel_block(int br_A, int bc_A, int br_B, int bc_B, int br_C, int bc_C,
                                   int M, int N, int K, double* A, double* B, double* C) {
    int i = 0, j = 0, k = 0;
    double VR1, VR2, VR3, VR4, VR5, VR6, VR7, VR8;
    // Using 2x2 blocks
    // TODO: modulo pre-fetching, make arr src, arr dest, copy from src to dest (malloc), pointers

    while (j <= N - 2) {
        i = 0;
        while (i <= M - 2) {
            VR1 = _access_memory_col(C, br_C, bc_C, i, j);
            VR2 = _access_memory_col(C, br_C, bc_C, i, j + 1);
            VR3 = _access_memory_col(C, br_C, bc_C, i + 1, j);
            VR4 = _access_memory_col(C, br_C, bc_C, i + 1, j + 1);


            for (k = 0; k < K; k++) {
                VR5 = _access_memory_col(B, br_B, bc_B, k, j);
                VR6 = _access_memory_col(B, br_B, bc_B, k, j + 1);
                // VR5 = _access_memory_row(B, br_B, bc_B, k, j);
                // VR6 = _access_memory_row(B, br_B, bc_B, k, j + 1);
                VR7 = _access_memory_col(A, br_A, bc_A, i, k);
                VR8 = _access_memory_col(A, br_A, bc_A, i + 1, k);
                // VR7 = _access_memory_row(A, br_A, bc_A, i, k);
                // VR8 = _access_memory_row(A, br_A, bc_A, i + 1, k);

                VR1 += VR5 * VR7;
                VR2 += VR6 * VR7;
                VR3 += VR5 * VR8;
                VR4 += VR6 * VR8;
            }
            _access_memory_col(C, br_C, bc_C, i, j) = VR1;
            _access_memory_col(C, br_C, bc_C, i, j + 1) = VR2;
            _access_memory_col(C, br_C, bc_C, i + 1, j) = VR3;
            _access_memory_col(C, br_C, bc_C, i + 1, j + 1) = VR4;

            i += 2;                     // advance by 2
        }
        j += 2;                         // advance by 2
    }
    // Using 1x1 tiles
    // printf("Test 4 N(%d) M(%d)\n", N, M); fflush(stdout);
    double VR56, VR78, VRC;


    if (N % 2 == 1) {                       // Has single col
        j = N - 1;
        for (i = 0; i < M; i ++) {
            VRC = _access_memory_col(C, br_C, bc_C, i, j);           // Mult M(i, k) * N(k, j)
            for (k = 0; k < K; k++) {
                VR56 = _access_memory_col(B, br_B, bc_B, k, j);
                VR78 = _access_memory_col(A, br_A, bc_A, i, k);
                // VR78 = _access_memory_row(A, br_A, bc_A, i, k);
                VRC += VR56 * VR78;
            }
            _access_memory_col(C, br_C, bc_C, i, j) = VRC;
        }
    }
    int N_even = (N % 2 == 1)? N - 1 :N;
    if (M % 2 == 1) {                       // Has single row
        i = M - 1;
        for (j = 0; j < N_even; j++) {
            VRC = _access_memory_col(C, br_C, bc_C, i, j);           // Mult M(i, k) * N(k, j), same
            for (k = 0; k < K; k++) {
                VR56 = _access_memory_col(B, br_B, bc_B, k, j);
                VR78 = _access_memory_col(A, br_A, bc_A, i, k);
                // VR78 = _access_memory_row(A, br_A, bc_A, i, k);
                VRC += VR56 * VR78;
            }
            _access_memory_col(C, br_C, bc_C, i, j) = VRC;
        }
    }
}



/* This routine performs a dgemm operation
 *  C := C + A * B
 * where A, B, and C are lda-by-lda matrices stored in column-major format.
 * On exit, A and B maintain their input values. */
void square_dgemm(int lda, double* A, double* B, double* C) {
    // Block MM: (M, N) = (M, K) * (N, K)
    int M = BLOCK_ROW_SIZE;
    int N = BLOCK_ROW_SIZE;
    int K = BLOCK_COL_SIZE;

    // Block size (br_X * bc_X) for do_tiling_kernel_block
    int br_C = M, bc_C = N;
    int br_A = M, bc_A = K;
    int br_B = K, bc_B = N;

    // Pad matrix with modulo
    int nb_row = (lda + BLOCK_ROW_SIZE - 1) / BLOCK_ROW_SIZE; // number of blocks
    int nb_col = (lda + BLOCK_COL_SIZE - 1) / BLOCK_COL_SIZE;
    int lda_row = nb_row * BLOCK_ROW_SIZE;
    int lda_col = nb_col * BLOCK_COL_SIZE;

    // Padded matrix size (ldar_X * ldac_x)
    int ldar_C = lda_row, ldac_C = lda_row;
    int ldar_A = lda_row, ldac_A = lda_col;
    int ldar_B = lda_col, ldac_B = lda_row;

    // Padded matrix block number (nbr_X * nbc_X)
    int nbr_C = nb_row, nbc_C = nb_row;
    int nbr_A = nb_row, nbc_A = nb_col;
    int nbr_B = nb_col, nbc_B = nb_row;

    double * A_new, * B_new, * C_new;
    // double * C_test_new, * A_test_new;

    // Re-arrange memory
    int A_COL = 1, B_COL = 1, C_COL = 1;
    int USE_BLOCK = 1;
    int TEST = 0;
    if (TEST > 0) {
        printf("Print A old\n"); fflush(stdout);
        _print_matrix(A, lda, lda, 1);
    }
    if (A_COL == 0) { A_new = _to_row_major(A, lda, lda, KEEP_OLD); } else { A_new = _copy(A, lda, lda); }
    // if (1 == 0) { A_test_new = _to_row_major(A, lda, lda, KEEP_OLD); } else { A_test_new = _copy(A, lda, lda); }
    if (B_COL == 0) { B_new = _to_row_major(B, lda, lda, KEEP_OLD); } else { B_new = _copy(B, lda, lda); }
    if (C_COL == 0) { C_new = _to_row_major(C, lda, lda, KEEP_OLD); } else { C_new = _copy(C, lda, lda); }
    // if (C_COL == 0) { C_test_new = _to_row_major(C, lda, lda, KEEP_OLD); } else { C_test_new = _copy(C, lda, lda); }

    if (USE_BLOCK) {
        if (TEST > 0) {
            printf("Print A\n"); fflush(stdout);
            _print_matrix(A_new, lda, lda, A_COL);
        }

        A_new = _flatten_memory_block(A_new, lda, lda, nbr_A, nbc_A, br_A, bc_A, A_COL);
        // A_test_new = _flatten_memory_block(A_test_new, lda, lda, nbr_A, nbc_A, br_A, bc_A, A_COL);
        B_new = _flatten_memory_block(B_new, lda, lda, nbr_B, nbc_B, br_B, bc_B, B_COL);
        C_new = _flatten_memory_block(C_new, lda, lda, nbr_C, nbc_C, br_C, bc_C, C_COL);

        if (TEST > 0) {
            printf("Print A flatten\n"); fflush(stdout);
            _print_matrix(A_new, lda, lda, A_COL);
            A_new = _unflatten_memory_block(A_new, lda, lda, nbr_A, nbc_A, br_A, bc_A, A_COL);
            printf("Print A unflatten\n"); fflush(stdout);
            _print_matrix(A_new, lda, lda, A_COL);
        }
    }

    if (TEST == 0) {
        for (int jb = 0; jb < nb_row; jb += 1) {
            for (int ib = 0; ib < nb_row; ib += 1) {
                for (int kb = 0; kb < nb_col; kb += 1) {
                    // A(i, k)/(M, N)  B(k, J)(M, K)  C (i, j)/(K, N)
                    double * A_block, * B_block, * C_block;
                    // double * C_test_block, * A_test_block;
                    if (USE_BLOCK > 0) {
                        int ldar_A = min(br_A, lda - ib * br_A), ldar_C = ldar_A;
                        int ldac_B = min(bc_B, lda - jb * bc_B), ldac_C = ldac_B;
                        int ldar_B = min(br_B, lda - kb * br_B), ldac_A = ldar_B;
                        M = ldar_A, N = ldac_B, K = ldac_A;
                        C_block = _jump_to_flatten_block_col(C_new, lda, lda, br_C, bc_C, ib, jb);
                        A_block = _jump_to_flatten_block_col(A_new, lda, lda, br_A, bc_A, ib, kb);
                        // A_block = _jump_to_flatten_block_row(A_new, lda, lda, br_A, bc_A, ib, kb);
                        B_block = _jump_to_flatten_block_col(B_new, lda, lda, br_B, bc_B, kb, jb);

                        // A_test_block = _jump_to_flatten_block_col(A_test_new, lda, lda, br_A, bc_A, ib, kb);
                        // C_test_block = _jump_to_flatten_block_col(C_test_new, lda, lda, br_C, bc_C, ib, jb);

                        // do_tiling_kernel_block(ldar_A, ldac_A, ldar_B, ldac_B, ldar_C, ldac_C, M, N, K, A_block, B_block, C_block);
                        // printf("print A\n"); fflush(stdout);
                        // _print_matrix(A, lda, lda, A_COL);
                        // printf("print A block ib(%d) kb(%d) diff(%d) %.04f\n", ib, kb, A_block - A_new, *A_block); fflush(stdout);
                        // _print_matrix(A_block, M, K, A_COL);
                        // printf("A test block ib(%d) kb(%d) diff(%d) %.04f\n", ib, kb, A_block - A_new, *A_block); fflush(stdout);
                        // _print_matrix(A_test_block, M, K, 1);

                        // printf("print B\n"); fflush(stdout);
                        // _print_matrix(B, lda, lda, B_COL);
                        // printf("B block kb(%d) jb(%d) diff(%d) %.04f\n", kb, jb, B_block - B_new, *B_block); fflush(stdout);
                        // _print_matrix(B_block, K, N, B_COL);

                        // printf("C block before ib(%d) jb(%d) diff(%d) %.04f\n", ib, jb, C_block - C_new, *C_block); fflush(stdout);
                        // _print_matrix(C_block, M, N, C_COL);
                        // printf("C test block before ib(%d) jb(%d) diff(%d) %.04f\n", ib, jb, C_block - C_new, *C_block); fflush(stdout);
                        // _print_matrix(C_test_block, M, N, 1);


                        if (M % 8 == 0) {
                            // do_block_avx_row(ldar_A, ldac_A, ldar_B, ldac_B, ldar_C, ldac_C, M, N, K, A_block, B_block, C_block);
                            do_block_avx_col(ldar_A, ldac_A, ldar_B, ldac_B, ldar_C, ldac_C, M, N, K, A_block, B_block, C_block);
                        } else {
                            // do_block_ijk_row(ldar_A, ldac_A, ldar_B, ldac_B, ldar_C, ldac_C, M, N, K, A_block, B_block, C_block);
                            do_block_ijk_col(ldar_A, ldac_A, ldar_B, ldac_B, ldar_C, ldac_C, M, N, K, A_block, B_block, C_block);
                            // do_block_jki(ldar_A, ldac_A, ldar_B, ldac_B, ldar_C, ldac_C, M, N, K, A_block, B_block, C_block);
                        }

                        // printf("C block after ib(%d) jb(%d) diff(%d) %.04f\n", ib, jb, C_block - C_new, *C_block); fflush(stdout);
                        // _print_matrix(C_block, M, N, C_COL);
                        // printf("C test block after ib(%d) jb(%d) diff(%d) %.04f\n", ib, jb, C_block - C_new, *C_block); fflush(stdout);
                        // _print_matrix(C_test_block, M, N, 1);

                        // C_test_block = _jump_to_col(C_test_new, lda, lda, br_C, bc_C, ib, jb);
                        // // A_block = _jump_to_col(A_new, lda, lda, br_A, bc_A, ib, kb);
                        // A_block = _jump_to_row(A_new, lda, lda, br_A, bc_A, ib, kb);
                        // B_block = _jump_to_col(B_new, lda, lda, br_B, bc_B, kb, jb);
                        // do_tiling_kernel_block(lda, lda, lda, lda, lda, lda, M, N, K, A_block, B_block, C_test_block);

                        // printf("C new jb(%d) ib(%d) kb(%d)\n", jb, ib, kb); fflush(stdout);
                        // _print_matrix(C_new, lda, lda, C_COL);
                        // printf("C test jb(%d) ib(%d) kb(%d)\n", jb, ib, kb); fflush(stdout);
                        // _print_matrix(C_test_new, lda, lda, C_COL);

                    } else {
                        C_block = _jump_to_col(C_new, lda, lda, br_C, bc_C, ib, jb);
                        A_block = _jump_to_col(A_new, lda, lda, br_A, bc_A, ib, kb);
                        B_block = _jump_to_col(B_new, lda, lda, br_B, bc_B, kb, jb);
                        do_tiling_kernel_block(lda, lda, lda, lda, lda, lda, M, N, K, A_block, B_block, C_block);
                    }
                }
            }
        }


        if (USE_BLOCK) {
            C_new = _unflatten_memory_block(C_new, lda, lda, nbr_C, nbc_C, br_C, bc_C, C_COL);
            // C_test_new = _unflatten_memory_block(C_test_new, lda, lda, nbr_C, nbc_C, br_C, bc_C, C_COL);
        }

        if (USE_BLOCK) {
            // printf("C new\n"); fflush(stdout);
            // _print_matrix(C_new, lda, lda, C_COL);
            // printf("C test\n"); fflush(stdout);
            // _print_matrix(C_test_new, lda, lda, C_COL);
            // free(C_test_new);
        }
        if (C_COL == 0) { C_new = _to_col_major(C_new, lda, lda, FREE_OLD); }

        memcpy(C, C_new, lda * lda * sizeof(double));
        // memcpy(C, C_test_new, lda * lda * sizeof(double));
    }

    free(A_new);
    // free(A_test_new);
    free(B_new);
    free(C_new);
    // free(C_test_new);
}

// /* This routine performs a dgemm operation
//  *  C := C + A * B
//  * where A, B, and C are lda-by-lda matrices stored in column-major format.
//  * On exit, A and B maintain their input values. */
// void square_dgemm_shivin(int lda, double* A, double* B, double* C) {
//     // For each block-column of B
//     for (int j = 0; j < lda; j += BLOCK_ROW_SIZE) {
//         // For each block-row of A
//         for (int i = 0; i < lda; i += BLOCK_ROW_SIZE) {
//             // Accumulate block dgemms into block of C
//             for (int k = 0; k < lda; k += BLOCK_COL_SIZE) {
//                 // Correct block dimensions if block "goes off edge of" the matrix
//                 int M = min(BLOCK_ROW_SIZE, lda - i);
//                 int N = min(BLOCK_ROW_SIZE, lda - j);
//                 int K = min(BLOCK_COL_SIZE, lda - k);
//                 // Perform individual block dgemm
//                 // do_block(lda, M, N, K, A + i + k * lda, B + k + j * lda, C + i + j * lda);
//                 if (M % 8 == 0) {
//                     // printf("Here %d %d %d \n", M, N, K);
//                     // fflush(stdout);
//                     do_block_avx(lda, M, N, K, A + i + k * lda, B + k + j * lda, C + i + j * lda);
//                 } else {
//                     do_block_jki(lda, M, N, K, A + i + k * lda, B + k + j * lda, C + i + j * lda);
//                 }
//                 // do_tiling_kernel_block(lda, M, N, K, A + i + k * lda, B + k + j * lda, C + i + j * lda);
//             }
//         }
//     }
// }

