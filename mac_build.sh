export CC=/usr/local/bin/x86_64-apple-darwin19-gcc-10
export CXX=/usr/local/bin/x86_64-apple-darwin19-g++-10

make clean && cmake -DCMAKE_BUILD_TYPE=Release  -DCMAKE_CXX_FLAGS=-I/usr/local/opt/openblas/include  -DCMAKE_C_COMPILER=/usr/local/bin/x86_64-apple-darwin19-gcc-10 -DCMAKE_CXX_COMPILER=/usr/local/bin/x86_64-apple-darwin19-g++-10 -DMAX_SPEED=44.8 .. && make -j 8


# make clean && cmake .. && make -j 8


# cmake -DCMAKE_BUILD_TYPE=Release  -DCMAKE_CXX_FLAGS=-I/usr/local/opt/openblas/include  -DCMAKE_C_COMPILER=/usr/local/bin/x86_64-apple-darwin19-gcc-10 -DCMAKE_CXX_COMPILER=/usr/local/bin/x86_64-apple-darwin19-g++-10 -DMAX_SPEED=44.8 .. && make -j 8
